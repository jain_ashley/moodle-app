package com.ashleyjain.moodleapp;

import android.app.Fragment;

/**
 * Created by saurabh on 19/2/16.
 */
public interface FragmentChangeListener {
    public void replaceFragment(Fragment FRAG);
}
